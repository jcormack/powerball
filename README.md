## Requirments
- Recommend running on node 5.x.x

## Getting Started
1. `npm install`
1. `npm run test` to run tests
1. `npm run dev` to run app and tests.

To see app go to `http://localhost:8080/`
