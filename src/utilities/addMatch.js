import calculateDifference from './calculateDifference';

export default function(match, league) {
  let teamOne;
  let teamTwo;
  let points;
  let homeRatingBump;
  let updatedLeague;
  let COUNTRY = match.venue.country;

  const TEAM_ONE_WIN = "A";
  const TEAM_TWO_WIN = "B";

  for (let i = 1; i < league.length; i++) {
    // add home bonus before calculations
    if (league[i].team.name === COUNTRY) {
      league[i].pts += 3;
    }

    if (league[i].team.name === match.teams[0].name) {
      teamOne = league[i];
    }
    else if (league[i].team.name === match.teams[1].name) {
      teamTwo = league[i];
    }
  }

  // work out rating difference
  points = calculateDifference(teamOne.pts, teamTwo.pts) / 10;

  switch (match.outcome) {
    case TEAM_ONE_WIN:
      teamOne.pts += 1 - points;
      teamTwo.pts -= 1 - points;
      break;
    case TEAM_TWO_WIN:
      teamOne.pts -= 1 + points;
      teamTwo.pts += 1 + points;
    default:
      break; // make sure switch statement doesn't get stuck
  }

  // remove home bonus after calculations
  switch (COUNTRY) {
    case teamOne.team.name:
      teamOne.pts -= 3;
      break;
    case teamTwo.team.name:
      teamTwo.pts -= 3;
      break;
    default:
      break; // make sure switch statement doesn't get stuck
  }

  function roundPoints(points) {
    return Math.round(points * 100) / 100
  }

  teamOne.pts = roundPoints(teamOne.pts);
  teamTwo.pts = roundPoints(teamTwo.pts);

  function isTeam(value) {
    let id = value.team.id;
    if ( teamOne.team.id === id || teamTwo.team.id === id) {
      return false;
    } else {
      return true;
    }
  }

  updatedLeague = league.filter(isTeam);
  updatedLeague.push(teamOne, teamTwo);

  // sort league by points
  updatedLeague.sort(
    function(a, b) {
      return b.pts - a.pts;
    }
  );

  // assign league positions
  for (let i = 0; i < updatedLeague.length; i++) {
    updatedLeague[i].pos = i + 1;
  }

  return updatedLeague;
}
