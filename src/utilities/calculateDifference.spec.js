import expect from 'expect';
import calculateDifference from './calculateDifference';

describe('calculateDifference', () => {
  it('should return the difference', () => {

    let val1 = 5;
    let val2 = 10;
    let expectedDifference = val2 - val1;

    const difference = calculateDifference(val2, val1);

    // return difference regardles off arg order
    expect(difference).toEqual(expectedDifference);
  });
});
