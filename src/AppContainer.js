import React from 'react';
import addMatch from './utilities/addMatch';


class AppContainer extends React.Component {
  constructor() {
    super(); // give the context of this to the component

    this.state = {
      league : [
        { "team": { "name": "Australia", "id": 32 }, "pos": 1, "pts": 54.23 },
        { "team": { "name": "New Zealand", "id": 62 }, "pos": 2, "pts": 54.00 },
        { "team": { "name": "France", "id": 2 }, "pos": 3, "pts": 52.95 },
        { "team": { "name": "England", "id": 1 }, "pos": 4, "pts": 52.32 },
        { "team": { "name": "Romania", "id": 24 }, "pos": 5, "pts": 43.50 }
      ]
    }
  }

  handleClick(e) {
    let match = {
      "matchId": 2524,
      "description": "Match 2",
      "venue": { "id": 900, "name": "Stadium", "city": "Paris", "country": "France" },
      "teams": [
        { "id": 2, "name": "France", "abbreviation": "FRA" },
        { "id": 1, "name": "England", "abbreviation": "ENG" }
      ],
      "scores": [
        19, 23
      ],
      "status": "C",
      "outcome": "B"
    };

    this.setState({ league: addMatch( match, this.state.league )});
  }

  render() {
    return (
      <div>
        <table>
          <thead>
            <tr>
              <td>Pos</td>
              <td>Team</td>
              <td>Pts</td>
            </tr>
          </thead>
          <tbody>
            {
              this.state.league.map(
                team =>
                <tr key={team.team.id}>
                  <td>{team.pos}</td>
                  <td>{team.team.name}</td>
                  <td>{team.pts}</td>
                </tr>
              )
            }
          </tbody>
        </table>
        <button onClick={(e) => this.handleClick(e)}>France vs England</button>
      </div>
    );
  }
}

export default (AppContainer);
